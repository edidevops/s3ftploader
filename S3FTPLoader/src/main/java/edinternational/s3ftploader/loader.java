/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edinternational.s3ftploader;

import com.amazonaws.regions.Regions;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class loader extends javax.swing.JFrame {

    /**
     * Creates new form loader
     */
    private String ErrCode = "1";
    private String ConfigFile = "";
    private String dst = "";
    private String src = "";
    private String bucket = "";
    private String keyName = "";
    private String accessKey = "";
    private String secretKey = "";
    private List<String> fileNames;
    private final ExecutorService executor;

    private S3 s3;
    private boolean upload = true;

    public loader(String[] args) {
        initComponents();
        setTitle("S3FTPLoader");
        logs_ta.setFocusable(false);
        setSpinnerVisibility(false);
        executor = Executors.newCachedThreadPool();
        jLabel2.setText("0%");
        if (args.length <= 0) {
            onfail("No Parameter found");
        }

        for (int f = 0; f < args.length; f++) {
            if (args[f].equalsIgnoreCase("-cfg")) {
                ConfigFile = args[f + 1];
                isFileDir(ConfigFile);
            } else if (args[f].equalsIgnoreCase("-dwn")) {
                //Getting the connection details                    
                upload = false;
            } else if (args[f].equalsIgnoreCase("-to")) {
                dst = args[f + 1];
                if(upload == false){
                   isFileDir(dst);
                }
                boolean hasfwd = dst.charAt(dst.length()-1) == '/';
                if(!hasfwd)
                    dst += "/";
            } else if (args[f].equalsIgnoreCase("-from")) {
                src = args[f + 1];
                isFileDir(src);
            } else if (args[f].equalsIgnoreCase("-b")) {
                bucket = args[f + 1];
            } else if (args[f].equalsIgnoreCase("-k")) {
                if (upload == false) {
                    keyName = args[f + 1];
                } else {
                    onfail("download param not set");
                }
            }
        }
        
        setResizable(false);
        setSize(397, 330);
        setVisible(true);
        
        fileLoc_lbl.setText("FTP trnsfr to bucket: "+bucket+" "+ dst+" from "+src);
        getAmazonCred();
        S3Thread s3Thread;
        if (upload) {
            fileNames = new ArrayList();
            File source = new File(src);
            if(source.isDirectory()){
            listFilesForFolder(source);
            s3Thread = new S3Thread(s3, upload, fileNames, bucket, dst, this);
            executor.submit(s3Thread);
            } else {
            fileNames.add(source.getPath());
            s3Thread = new S3Thread(s3, upload, fileNames, bucket, dst, this);
            executor.submit(s3Thread);
            }
        } else {
            if (keyName.length() == 0){
                s3Thread = new S3Thread(s3, upload, src, bucket, dst, this);
                executor.submit(s3Thread);
            }else{
                s3Thread = new S3Thread(s3, upload, src, bucket, dst, keyName, this);
                executor.submit(s3Thread);
            }
        }
    }
    
    private void listFilesForFolder( File folder) {
    for (final File fileEntry : folder.listFiles()) {
        if (fileEntry.isDirectory()) {
            listFilesForFolder(fileEntry);
        } else {
            fileNames.add(fileEntry.getPath());
        }
    }
}

    private void getAmazonCred() {
        File sourcefile = new File(ConfigFile);
        try {
                BufferedReader in = new BufferedReader(new FileReader(sourcefile));
                String str = null;
                Regions regions = null;
                while ((str = in.readLine()) != null) {
                    if (str.contains("accessKey")){
                        accessKey = getPropValue(str);
                    } else if(str.contains("secretKey")){
                        secretKey = getPropValue(str);
                    } else if (str.contains("region")){
                        String region = getPropValue(str);
                        if (region.equals("AP_EAST_1")){
                            regions = Regions.AP_EAST_1;
                        } else if (region.equals("AP_NORTHEAST_1")){
                            regions = Regions.AP_NORTHEAST_1;
                        } else if (region.equals("AP_NORTHEAST_2")){
                            regions = Regions.AP_NORTHEAST_2;
                        }else if (region.equals("AP_SOUTHEAST_1")){
                            regions = Regions.AP_SOUTHEAST_1;
                        }else if (region.equals("AP_SOUTHEAST_2")){
                            regions = Regions.AP_SOUTHEAST_2;
                        }else if (region.equals("AP_SOUTH_1")){
                            regions = Regions.AP_SOUTH_1;
                        }else if (region.equals("CA_CENTRAL_1")){
                            regions = Regions.CA_CENTRAL_1;
                        }else if (region.equals("EU_NORTH_1")){
                            regions = Regions.EU_NORTH_1;
                        }else if (region.equals("EU_WEST_1")){
                            regions = Regions.EU_WEST_1;
                        }else if (region.equals("EU_WEST_2")){
                            regions = Regions.EU_WEST_2;
                        }else if (region.equals("EU_WEST_3")){
                            regions = Regions.EU_WEST_3;
                        }else if (region.equals("GovCloud")){
                            regions = Regions.GovCloud;
                        }else if (region.equals("ME_SOUTH_1")){
                            regions = Regions.ME_SOUTH_1;
                        }else if (region.equals("SA_EAST_1")){
                            regions = Regions.SA_EAST_1;
                        }else if (region.equals("US_EAST_2")){
                            regions = Regions.US_EAST_2;
                        }else if (region.equals("US_GOV_EAST_1")){
                            regions = Regions.US_GOV_EAST_1;
                        }else if (region.equals("US_WEST_1")){
                            regions = Regions.US_WEST_1;
                        }else if (region.equals("US_WEST_2")){
                            regions = Regions.US_WEST_2;
                        }else if (region.equals("CN_NORTHWEST_1")){
                            regions = Regions.CN_NORTHWEST_1;
                        }else if (region.equals("CN_NORTH_1")){
                            regions = Regions.CN_NORTH_1;
                        }else if (region.equals("DEFAULT_REGION")){
                            regions = Regions.DEFAULT_REGION;
                        }else if (region.equals("EU_CENTRAL_1")){
                            regions = Regions.EU_CENTRAL_1;
                        }
                    }
                }
                
                if (accessKey.equals("")){
                  onfail("Could not find accessKey");
                }
                if (secretKey.equals("")){
                 onfail("Could not find secretKey" );
                }
                if(regions == null){
                 onfail("Could not find regions");
                }
                s3 = new S3( accessKey, secretKey, regions, this);
                in.close();
            
        } catch (FileNotFoundException ex) {
               onfail(ex.getMessage());
        } catch (IOException ex) {
                onfail(ex.getMessage());
        }
    }

    
    private String getPropValue(String property){
        property = property.substring(property.indexOf("=")+1, property.length()).trim();
        return property;
    }
    
    private boolean isFileDir(String path){
        File file = new File(path);
        if(file.isFile())
        {
            return true;
        } else if (file.isDirectory()){
            return true;
        } else {
            onfail("Path - "+ path + " - does not exist");
            return false;
        }
        
    }
    
    public void setSpinnerVisibility(boolean isVisible){
        jLabel1.setVisible(isVisible);
    }
    
    private void onfail(String error){
        System.out.print(error);
        exitForm(1);
    }

    private void exitForm(int code) {
        if (code == 0) {
            dispose();
        } else {
            System.exit(1);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileLoc_lbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        logs_ta = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().setLayout(null);
        getContentPane().add(fileLoc_lbl);
        fileLoc_lbl.setBounds(10, 10, 360, 30);

        logs_ta.setColumns(20);
        logs_ta.setRows(5);
        jScrollPane1.setViewportView(logs_ta);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(10, 50, 370, 210);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ezgif.com-resize.gif"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(100, 233, 200, 90);

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(300, 264, 70, 30);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 400, 300);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(loader.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(loader.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(loader.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(loader.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                loader loader_ = new loader(args);
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel fileLoc_lbl;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTextArea logs_ta;
    // End of variables declaration//GEN-END:variables
}

class S3Thread extends Thread{

    private S3 s3;
    private boolean upload;
    private List<String> filenames;
    private String bucket;
    private String dst;
    private String src;
    private String keyname = "";
    private loader lo;
    
    public S3Thread(S3 s3, boolean isUpload, List<String> filenames, String bucket, String dst, loader lo) {
        this.s3 = s3;
        this.upload = isUpload;
        this.bucket = bucket;
        this.dst = dst;
        this.filenames = filenames;
        this.lo = lo;
    }
    
    public S3Thread(S3 s3, boolean isUpload, String src, String bucket, String dst, loader lo) {
        this.s3 = s3;
        this.upload = isUpload;
        this.bucket = bucket;
        this.dst = dst;
        this.src = src;
        this.lo = lo;
    }
    
    public S3Thread(S3 s3, boolean isUpload, String src, String bucket, String dst, String keyname, loader lo) {
        this.s3 = s3;
        this.upload = isUpload;
        this.bucket = bucket;
        this.dst = dst;
        this.src = src;
        this.keyname = keyname;
        this.lo = lo;
    }
    

    @Override
    public void run() {
        lo.setSpinnerVisibility(true);
        if(upload){
            s3.upload(filenames, bucket, dst);
        }else{
            if(keyname.length() == 0){
                            s3.download(bucket, src, dst);
            }else{
                            s3.download(bucket, keyname, src, dst);
            }
        }
        lo.setSpinnerVisibility(false);

    }
}