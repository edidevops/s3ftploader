/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edinternational.s3ftploader;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTextArea;
import org.apache.commons.io.FileUtils;

public class S3 {

    private final AmazonS3 s3client;
    private int counter = 0;
    private loader lo;
    
    public S3( String accessKey, String secretKey, Regions region,  loader lo) {
        this.lo = lo;

       AWSCredentials credentials = new BasicAWSCredentials(
                accessKey,
                secretKey);

        s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(region)
                .build();
    }

    public void upload(List<String> filenames, String bucketName, String dst) {
        int max = filenames.size();
        try{
        for (String filename : filenames) {
            File file = new File(filename);
            s3client.putObject(bucketName, dst+file.getName(), file).toString();
            lo.logs_ta.append("Upload successful: "+file+" -> "+bucketName+"/"+dst+"\n");
            counter++;
           lo.jLabel2.setText(((counter/max)*100)+"%");

        }
        counter = 0;
        }catch(AmazonS3Exception ex){
            lo.logs_ta.append(ex.getMessage()+"\n");
        }
    }

    public void download(String bucketName, String src, String dst) {
         try {
        S3Object s3object = s3client.getObject(bucketName, src);
        S3ObjectInputStream inputStream = s3object.getObjectContent();
       
            FileUtils.copyInputStreamToFile(inputStream, new File(dst));
             lo.logs_ta.append("\nDownload successful: "+bucketName+"/"+src+" -> "+dst+src+"\n");
             counter++;
             lo.jLabel2.setText(((counter/1)*100)+"%");

        } catch (IOException ex) {
            lo.logs_ta.append(ex.getMessage()+"\n");
        } catch(AmazonS3Exception ex){
            lo.logs_ta.append(ex.getMessage()+"\n");
        }
                counter = 0;

    }

    public void download(String bucketName, String folderKey, String src, String dst) {
                 try {

        ListObjectsRequest listObjectsRequest
                = new ListObjectsRequest()
                        .withBucketName(bucketName)
                        .withPrefix(folderKey + "/");

        List<String> keys = new ArrayList();

        ObjectListing objects = s3client.listObjects(listObjectsRequest);
        for (;;) {
            List<S3ObjectSummary> summaries = objects.getObjectSummaries();
            if (summaries.size() < 1) {
                break;
            }
            summaries.forEach(s -> keys.add(s.getKey()));
            objects = s3client.listNextBatchOfObjects(objects);
        }
                int max = keys.size();
        for (String key : keys) {
            S3Object s3object = s3client.getObject(bucketName, key);
            S3ObjectInputStream inputStream = s3object.getObjectContent();
            try {
                FileUtils.copyInputStreamToFile(inputStream, new File(dst + key));
                lo.logs_ta.append("\nDownload successful: "+bucketName+"/"+key+" -> "+dst+key+"\n");
                lo.jLabel2.setText(((counter/max)*100)+"%");
             counter++;
            } catch (IOException ex) {
                lo.logs_ta.append(ex.getMessage()+"\n");
            }
        }
        lo.jLabel2.setText(((counter/max)*100)+"%");
        } catch(AmazonS3Exception ex){
            lo.logs_ta.append(ex.getMessage()+"\n");
        }
                 
        counter = 0;
    }
    

}
